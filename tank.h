#ifndef _TANK_H
#define _TANK_H

using namespace std;

class TANK
{
private:
	double capacity;
	double fuel_quantitiy;
	bool broken;
	bool valve;
public:
	TANK();
	~TANK();
	bool openValve(int);
	int addFuelTank(double);
	void listFuelTanks();
	void removeFuelTank(int);
	bool connectFuelTank(int);
	bool disconnectFuelTank(int);
	bool closeValve(int);
	bool breakFuelTank(int);
	bool reapirFuelTank(int);

};

#endif