#pragma once
class engine {

private:
	double fuel_per_second;
	bool status;
public:
	engine();
	void startEngine();
	void stopEngine();
	void absorbFuel(double);
	void giveBackFuel(double);



};